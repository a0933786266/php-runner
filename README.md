# php-runner

## build image
docker build --build-arg PHP_VER=7.2 -t yutang:php-runner .

## tag
docker tag php-runner:latest yutang:php-runner:7.2

## push
docker push yutang/php-runner:7.2
