ARG PHP_VER
FROM php:$PHP_VER-cli-alpine
LABEL name="php-runner"
LABEL version="latest"
RUN apk update \
    && apk add --no-cache git openssh libmemcached-libs zlib sqlite \
    && apk --no-cache add --virtual .memcached-deps zlib-dev libmemcached-dev cyrus-sasl-dev \
    && apk --no-cache add --virtual .phpize-deps ${PHPIZE_DEPS} \
    && docker-php-ext-install pdo_mysql \
    && pecl install redis-5.2.1 \
    && pecl install memcached-3.1.5 \
    && pecl install xdebug-2.9.6 \
    && docker-php-ext-enable redis memcached xdebug \
    && rm -rf /tmp/* \
    && apk del .memcached-deps .phpize-deps

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Requirement of utapass-cron-billing
RUN apk add gmp-dev freetype-dev libjpeg-turbo-dev libpng-dev libzip-dev php$PHP_VER-pdo_sqlite \
    && docker-php-ext-install gmp gd zip
# Requirement of global-member-lib
RUN docker-php-ext-install bcmath
#RUN composer global require hirak/prestissimo
WORKDIR /code
